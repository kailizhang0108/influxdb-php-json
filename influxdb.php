<?php
require __DIR__ . '/vendor/autoload.php';
use InfluxDB2\Client;
use InfluxDB2\Model\BucketRetentionRules;
use InfluxDB2\Model\Organization;
use InfluxDB2\Model\PostBucketRequest;
use InfluxDB2\Service\BucketsService;
use InfluxDB2\Service\OrganizationsService;


class Influxdb
{
    private static $_ins;
    private static $_client;

    // should get from config
    private static $host = "http://localhost:8086";
    private static $organization = "cs";
    private static $bucket = "sodium";
    private static $token = 'pXj10sq9RWB4AYbmJhm79SCZV-_ogNoWOLRIzIijziYdruMvKYna16mKm2Uvf-FjDYe3eLZRF3L61HQMg84dLw==';

    private function __construct() {}

    public static function getIns()
    {
        if (!(self::$_ins instanceof self)) {
            self::$_ins = new self();
        }
        return self::$_ins;
    }

    private function __clone() {}

    private function __wakeup() {}

    public function getBucket() { return self::$bucket; }

    public function connect()
    {
        self::$_client = new Client([
            "url" => self::$host,
            "token" => self::$token,
            "bucket" => self::$bucket,
            "org" => self::$organization,
            "precision" => InfluxDB2\Model\WritePrecision::S
        ]);
        return self::$_client;
    }

    public function readRecord($client, $query) {
        $queryApi = $client->createQueryApi();
        return $queryApi->query($query);
    }
}

