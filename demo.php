<?php

require "influxdb.php";

function isValidityQueryParams($bucket, $start, $measurement, $filterArr) {
    if ("" == $bucket || "" == $start || "" == $measurement || false == is_array($filterArr)) {
        return false;
    }
    return true;
}

function packQuery($bucket, $start, $end, $measurement, $filterArr) {
    if (!isValidityQueryParams($bucket, $start, $measurement, $filterArr)) {
        return "";
    }

    $filterStr = "";
    foreach ($filterArr as $key => $value) {
        if ($key == 0){
            $filterStr .=  "r[\"_field\"] == \"" . $value . "\"";
            continue;
        }
        $filterStr .=  " or r[\"_field\"] == \"" . $value . "\"";
    }

    if (empty($end)) {
        return "from(bucket: \"".$bucket."\") |>
                range(start: ".$start.") |>
                filter(fn: (r) => r[\"_measurement\"] == \"$measurement\") |>
                filter(fn: (r) => ".$filterStr.")";
    } else {
        return "from(bucket: \"".$bucket."\") |>
                range(start: ".$start.", stop: -".$end.") |>
                filter(fn: (r) => r[\"_measurement\"] == \"$measurement\") |>
                filter(fn: (r) => ".$filterStr.")";
    }
}

$res = Influxdb::getIns();
$client = $res->connect();
$query = packQuery($res->getBucket(),"-3d", "-3m", "chess", ["chess_primarymarket_btcb_balance", "chess_fund_btcb_balance"]);
$tables = $res->readRecord($client, $query);

$data = [];
foreach ($tables as $table) {
    $dataItem = [];
    foreach ($table->records as $record) {
        $item = [];
        $item["data"] = $record->getValue();
        $item["timestamp"] = (formatDate($record->getTime()));
        array_push($dataItem, $item);
    }
    array_push($data, $dataItem);
}


function formatDate($date) {
    $prefixArr = explode('T',$date);
    $suffixArr = explode('.',$prefixArr[1], 2);

    return strtotime($prefixArr[0]. " " . $suffixArr[0]);
}

echo json_encode( $data, JSON_PRETTY_PRINT ) ;
